import PageTemplate from '../../components/pageTemplate/PageTemplate';
import IconButton from '../../components/iconButton/IconButton';
import HeadingTitle from '../../components/headingTitle/HeadingTitle';
import ActionButton from '../../components/actionButton/ActionButton';

import { faArrowRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import { faLanguage } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import Col from 'react-bootstrap/Col';

import './profile.css';
import withAuth from '../../hoc/withAuth';
import { storageDelete, storageRead, storageSave } from '../../utils/storage';
import { deleteTranslations } from '../../api/user';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import { useState } from 'react';

const Profile = () => {
    const user = storageRead(STORAGE_KEY_USER);

    const [ translations, setTranslations ] = useState(user.translations);


    const profileTitle = `Hey, ${user.username}!`;
    
    // handle users without translations
    const profileSubtitle = user.translations.length > 0 ? 
        'Here are your last 10 translations:'
        : 'You don\'t have any translations yet!';

    const translationList = (
        <div className="list-box">
            { translations.slice(0,10).map((tr, index) => {
                // only first 10 translations
                return (
                    <div key={`translation-${index}`} className="list-item">  
                        {tr}
                    </div>
                );
            })}
        </div>
    );

    const logout = () => {
        if (window.confirm('Are you sure you want to logout?')) {
            storageDelete(STORAGE_KEY_USER)
            window.location.reload(true)
        }
    }

    const deleteUserTranslations = async () => {
        if (window.confirm("Are you sure you want to delete your translations?")) {
            const [error, updatedUser] = await deleteTranslations(user)

            if (error !== null) {
                alert(error.message);
            }
            
            if (updatedUser !== null) {
                storageSave(STORAGE_KEY_USER, updatedUser);
                setTranslations([]);
            }
        }
    }

    return (
        <PageTemplate>
            <div style={{ maxWidth: "1024px", margin: "0 auto" }}>
                <div className="buttons">
                    <NavLink to="/translate">
                        <IconButton buttonText="Translate" icon={faLanguage} />
                    </NavLink>
                    <NavLink to="/">
                        <IconButton buttonText="Logout" icon={faArrowRightFromBracket} onClick={logout}  />
                    </NavLink>
                </div>
                <div className="heading-box">
                    <HeadingTitle title={profileTitle} subtitle={profileSubtitle} />
                </div>
                {translations.length > 0 ?
                    // display only if user has translations
                    <Col className="profile-container">
                        {translationList}
                        <div className="delete-btn">
                            <ActionButton buttonText="CLEAN UP" type="button" onClick={deleteUserTranslations} />
                        </div>
                    </Col>
                : null}
            </div>
        </PageTemplate>
    )
}

export default withAuth(Profile)