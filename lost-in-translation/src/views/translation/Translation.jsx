import Row from "react-bootstrap/Row";

import PageTemplate from "../../components/pageTemplate/PageTemplate";
import ActionButton from '../../components/actionButton/ActionButton';
import IconButton from "../../components/iconButton/IconButton";
import withAuth from "../../hoc/withAuth";

import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import { useState } from "react";

import { storageRead, storageSave } from "../../utils/storage";
import { addTranslation } from "../../api/user";
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import './translation.css';

const Translation = () => {
    // saved after pressing the button
    const [translation, setTranslation] = useState("");
    // collects the input value "on the fly"
    const [inputText, setInputText] = useState("");

    // collect the input value and set it as a new translation sentence
    // here we can use API call to add new translation to the user ref
    const translate = async () => {
        const invalidCharacters = inputText.match(/[^a-zA-Z\s]/gm);
        if (invalidCharacters) {
            // invalid characters
            alert(`Invalid characters (${invalidCharacters.join(', ')}). Only letters are allowed`)
        } else {
            // api call
            const user = storageRead(STORAGE_KEY_USER);
            const [error, updatedUser] = await addTranslation(user, inputText);

            if (error !== null) {
                alert(error.message);
            }
            if (updatedUser !== null) {
                // use the response to update localStorage
                storageSave(STORAGE_KEY_USER, updatedUser);
                // update the local state with the updated user
                setTranslation(inputText);
            }
        }
    }

    return (
        <>
            <PageTemplate>
                <div className="translate-box">
                    <div className="buttons">
                        <NavLink to="/profile">
                            <IconButton buttonText="Profile" icon={faUserCircle} />
                        </NavLink>
                    </div>
                    <div className="input-form">
                        <textarea
                            placeholder="Your sentence in English..."
                            className="input-field"
                            rows={5}
                            value={inputText}
                            onChange={(e) => setInputText(e.target.value)}
                        />
                        <ActionButton buttonText="TRANSLATE" onClick={translate}/>
                    </div>
                    {translation ? 
                    <Row className="translated-box">
                        {translation.split("").map((x, index) => {
                            if (x === " ") {
                                return (
                                    <span key={`space-${index}`} className="space-sign" />
                                );
                            }
                            const img = require(`../../assets/individual_signs/${x.toLowerCase()}.png`);
                            return (
                                <img key={`img-${x}-${index}`} src={img} alt={`sign-${x}`} className="sign-image" />
                            );
                        })}
                    </Row> : null}
                </div>
            </PageTemplate>
        </>
    );
}

export default withAuth(Translation)