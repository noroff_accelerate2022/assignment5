import Col from 'react-bootstrap/Col';

import HeadingTitle from "../../components/headingTitle/HeadingTitle";
import PageTemplate from "../../components/pageTemplate/PageTemplate";
import LoginForm from  '../../components/Login/LoginForm'

import './login.css';

const Login = () => {
    const title = "Welcome!";
    const subtitle = "Please enter your name to continue:";

    return (
        <>
            <PageTemplate>
                <Col className="login-box">
                    <HeadingTitle title={title} subtitle={subtitle} />
                    <LoginForm />
                </Col>
            </PageTemplate>
        </>
    );
}

export default Login