import { Routes, Route, BrowserRouter } from 'react-router-dom';

import Login from './views/login/Login';
import Profile from './views/profile/Profile';
import Translation from './views/translation/Translation';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Routes>
          <Route path="/" element={< Login />} />
          <Route path="/profile" element={< Profile />} />
          <Route path="/translate" element={< Translation />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
