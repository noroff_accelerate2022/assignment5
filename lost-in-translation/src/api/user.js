import { createHeaders } from './index'

const apiUrl = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}/translations/?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [ null, data ]
    }
    catch (error) {
        return [ error.message, [] ]
    }
}

const createUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}/translations`, {
            method: 'POST', //create resource
            headers: createHeaders(),
            body: JSON.stringify({
                username: username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('Could not create user with username.' + username)
        }
        const data = await response.json();
        return [ null, data ]
    }
    catch (error) {
        return [ error.message, [] ]
    }
}

export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)

    if (checkError != null) {
        return [ checkError, null ]
    }
    
    if (user.length > 0) {
        return [ null, user.pop() ]
    }

    return await createUser(username)
}

export const addTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/translations/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation],
            })
        })
        if (!response.ok) {
            throw new Error('Could not update translations history')
        }
        const data = await response.json();
        return [ null, data ]
    } catch (error) {
        return [ error.message, null ]
    }
}

export const deleteTranslations = async (user) => {
    try {
        const response = await fetch(`${apiUrl}/translations/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [],
            })
        })
        if (!response.ok) {
            throw new Error('Could not update translations history')
        }
        const data = await response.json();
        return [ null, data ]
    } catch (error) {
        return [ error.message, null ]
    }
}