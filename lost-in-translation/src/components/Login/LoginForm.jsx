import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

import ActionButton from '../actionButton/ActionButton';

import './loginForm.css';

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    //hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    //local state
    const [ loading, setLoading ] = useState(false)   
    const [ apiError, setApiError ] = useState(null)    

    //side effects
    useEffect(() => {
        if (user !== null) {
            navigate('profile')
        }
    }, [ user, navigate ])

    //handlers
    const onSubmit = async ({ username }) => {
            setLoading(true)
            const [ error, userResponse ] = await loginUser(username)
            if (error !== null) {
                setApiError(error)
            }
            if (userResponse !== null) {
                storageSave(STORAGE_KEY_USER, userResponse)
                setUser(userResponse)
            }
            setLoading(false)
        }

    //render functions
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if (errors.username.type === 'minLength') {
            return <span>The username requires at least three characters</span>
        }
    }) ()

    return (
        <div className="form-box">
            <form onSubmit= { handleSubmit(onSubmit) }>
                <fieldset className="form-fields">
                    <input
                    className="name-input" 
                    type="text" 
                    placeholder="username"
                    { ...register("username", usernameConfig) } 
                    />
                    <div className='error'>
                        {errorMessage}
                    </div>
                </fieldset>

                <div className="form-button">
                    <ActionButton type="submit" buttonText="Continue" disabled={loading} />
                </div>
                { loading && <p>Logging in...</p> }
                { apiError && <p>{ apiError }</p> }

            </form>
        </div>
    )
}

export default LoginForm