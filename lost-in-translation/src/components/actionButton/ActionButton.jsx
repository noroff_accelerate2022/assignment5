import Button from 'react-bootstrap/Button';

import './actionButton.css';

const ActionButton = (props) => {
    const { buttonText, onClick, disabled=false, type='submit' } = props;

    return (
        <Button type={type} disabled={disabled} onClick={onClick} className="action-button">
            {buttonText}
        </Button>
    );
}

export default ActionButton;