import './headingTitle.css';

const HeadingTitle = (props) => {
    const { title, subtitle } = props;

    return (
        <div className="box">
            <div className="heading-title">
                {title}
            </div>
            <div className="heading-subtitle">
                {subtitle}
            </div>
        </div>
    );
}

export default HeadingTitle