import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/esm/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './iconButton.css';

// icons reference
// solid: https://github.com/FortAwesome/Font-Awesome/tree/master/js-packages/%40fortawesome/free-solid-svg-icons
// regular: https://github.com/FortAwesome/Font-Awesome/tree/master/js-packages/%40fortawesome/free-regular-svg-icons

const IconButton = (props) => {
    const { buttonText, icon, size="xl", onClick } = props;

    return (
        <Button onClick={onClick} className="icon-button">
            {buttonText}
            <FontAwesomeIcon className="icon" size={size} icon={icon}/>
        </Button>
    );
}

export default IconButton;