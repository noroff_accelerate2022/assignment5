// a dummy component containing the page background,
// won't change on any page
import Container from 'react-bootstrap/Container';

import './pageTemplate.css';

const PageTemplate = (props) => {
    const { children } = props;

    return (
        <Container className="container">
            <div className="circle" />
            <div className="square" />
            <div className="triangle" />
            <div className="small-circle" />
            <div className='title'>
                LOST IN<br />
                TRANSL<br/>
                ATION
            </div>
            {children}
        </Container>
    );
}

export default PageTemplate