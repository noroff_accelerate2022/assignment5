import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

// redirect to login page if session is empty
const withAuth = Component => props => {
    const { user } = useUser()
    if (user !== null) {
        return <Component {...props} />
    } else{
        return <Navigate to="/" />
    }
}

export default withAuth