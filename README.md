# Assignment 5 - Lost in Translation

Project's wireframes can be found [here](https://www.figma.com/file/Fq65ndn5WEcw5D8ZZK0X1S/assignment5?node-id=1%3A2).

This a simple React app that can be accessed [here](https://lost-in-translation-wermel.herokuapp.com/) 

It uses `React Bootstrap` and `Font Awesome` icons.

Lost in translation is a tool that let's you translate English sentences to American Sign Language.

> **Only** letters are allowed in the input.

Log in using your username. If it's not first time you're logging in, you will be able to access your last 10 translations on the `Profile` page. 

## Development

Remember to run `npm install` in order to install all necessary dependencies.

The API key, as well as API url should be stored in an `.env` file.

Run the application locally using `npm run dev`